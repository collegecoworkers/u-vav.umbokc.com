<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'SiteController@Index')->name('/');

Route::get('/confirm', 'SiteController@Confirm')->name('/confirm');
Route::get('/privacy', 'SiteController@Privacy')->name('/privacy');
Route::get('/test', 'SiteController@Test')->name('/test');

Route::post('/search', 'SiteController@Search')->name('/search');
Route::get('/oauth', 'SiteController@Oauth')->name('/oauth');
Route::get('/view/{id}', 'SiteController@View')->name('/view/{id}');
Route::get('/vk/view/{id}', 'SiteController@ViewVK')->name('/vk/view/{id}');
Route::get('/tw/view/{id}', 'SiteController@ViewTW')->name('/tw/view/{id}');
Route::get('/fb/view/{id}', 'SiteController@ViewFB')->name('/fb/view/{id}');
Route::get('/in/view/{id}', 'SiteController@ViewIN')->name('/in/view/{id}');
Route::get('/profile', 'SiteController@Profile')->name('/profile');

Route::get('/users', 'UserController@Index')->name('/users');
Route::get('/user/add', 'UserController@Add')->name('/user/add');
Route::get('/user/edit/{id}', 'UserController@Edit')->name('/user/edit/{id}');
Route::get('/user/delete/{id}', 'UserController@Delete')->name('/user/delete/{id}');
Route::post('/user/create', 'UserController@Create')->name('/user/create');
Route::post('/user/update/{id}', 'UserController@Update')->name('/user/update/{id}');
