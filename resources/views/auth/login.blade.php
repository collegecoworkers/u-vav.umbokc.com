@extends('../layouts.app')
@section('content')
<br>
<br>
<br>
<br>
<br>
<div class="col-md-4 col-md-offset-4">
	<div class="login-wrapper" style="position: relative;">
		<div class="box">
			<form class="content-wrap" method="post" action="{{ route('login') }}">
				{{ csrf_field() }}
				<h6>Вход</h6>
				<input class="form-control" type="text" name="email" placeholder="Email">
				@if ($errors->has('email'))<span class="help-block"><strong c#0>{{ $errors->first('email') }}</strong></span> @endif
				<input class="form-control" type="password" name="password" placeholder="Пароль">
				@if ($errors->has('password'))<span class="help-block"><strong c#0>{{ $errors->first('password') }}</strong></span> @endif
				<div class="action">
					<input type="submit" class="btn btn-primary signup" value="Войти" />
				</div>                
			</form>
		</div>

		<div class="already">
			<a href="{{ route('register') }}">Регистрация</a>
		</div>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
	</div>
</div>
@endsection
