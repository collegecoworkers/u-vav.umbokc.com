@extends('../layouts.app')
@section('content')
<br>
<br>
<br>
<br>
<br>
<div class="col-md-4 col-md-offset-4">
  <div class="login-wrapper" style="position: relative;">
    <div class="box">
      <form class="content-wrap" method="post" action="{{ route('register') }}">
        {{ csrf_field() }}
        <h6>Регистрация</h6>
        <input class="form-control" type="text" name="full_name" placeholder="Имя">
        @if ($errors->has('full_name'))<span class="help-block"><strong c#0>{{ $errors->first('full_name') }}</strong></span> @endif
        <input class="form-control" type="text" name="name" placeholder="Логин">
        @if ($errors->has('name'))<span class="help-block"><strong c#0>{{ $errors->first('name') }}</strong></span> @endif
        <input class="form-control" type="text" name="email" placeholder="Email">
        @if ($errors->has('email'))<span class="help-block"><strong c#0>{{ $errors->first('email') }}</strong></span> @endif
        <input class="form-control" type="password" name="password" placeholder="Пароль">
        @if ($errors->has('password'))<span class="help-block"><strong c#0>{{ $errors->first('password') }}</strong></span> @endif
        <input class="form-control" type="password" name="password_confirmation" placeholder="Пароль еще раз">
        @if ($errors->has('password_confirmation'))<span class="help-block"><strong c#0>{{ $errors->first('password_confirmation') }}</strong></span> @endif
        <div class="action">
          <input type="submit" class="btn btn-primary signup" value="Зарегистрироваться" />
        </div>                
      </form>
    </div>

    <div class="already">
      <a href="{{ route('login') }}">Войти</a>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
  </div>
</div>
@endsection
