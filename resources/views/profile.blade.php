@extends('layouts.app')
@section('content')
<div class="col-md-12">
	<div class="content-box-large">
		<div class="panel-heading">
			<div class="panel-title">Список ваших запросов</div>
		</div>
		<div class="panel-body">
			<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>Имя</th>
						<th>Найдено пользователей</th>
						<th>Действия</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($data as $item)
						<tr>
							<td>{{ $item->id }}</td>
							<td>{{ $item->name }}</td>
							<td>{{ $item->countAllFs() }}</td>
							<td><a href="{{ '/view/' . $item->id }}"><i class="fa fa-eye"></i></a></td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection
