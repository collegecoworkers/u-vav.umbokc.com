@extends('layouts.app')
@section('content')
	<div class="col-md-12">
		<div class="content-box-large">
			<div class="box" ta:c>
				<h1>Поиск людей <br> по социальным сетям</h1>
				<p class="sub-title" p:h:big>
					{{ config('app.name', 'Laravel') }} — это система для поиска людей, которая находит информацию из социальных сетей и объединяет ее в простые профили, чтобы помочь вам благополучно найти и узнать информацию о людях.
				</p>
				<br>
				<br>
				<br>
				<br>
				<div class="col-lg-8 col-md-offset-2">
					<form class="input-group form" method="post" action="{{ route('/search') }}">
						{{ csrf_field() }}
						<input type="text" class="form-control" name="name" placeholder="Имя и фамилия..." required="" autocomplete="off">
						<span class="input-group-btn">
							<button class="btn btn-primary" type="submit">Найти</button>
						</span>
					</form>
				</div>
				<br>
				<br>
				<br>
				<br>
				<br>
			</div>
		</div>
	</div>
@endsection
