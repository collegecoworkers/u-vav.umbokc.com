@extends('layouts.app')
@section('content')
<style>
[c\#vk]{ color: #5181b8 }
[c\#tw]{ color: #1da1f2 }
[c\#fb]{ color: #4267b2 }
.active
</style>
<div class="col-md-3">
	<div class="sidebar content-box" style="display: block;">
		<ul class="nav">
			<li class=" {{ $active == '*' ? 'current' : '' }} "><a href="{{ route('/view/{id}', ['id' => $id]) }}"><i class="fa fa-list"></i> Все</a></li>
			<li class=" {{ $active == 'vk' ? 'current' : '' }} "><a href="{{ route('/vk/view/{id}', ['id' => $id]) }}"><i c#vk class="fa fa-vk"></i> Вконтакте</a></li>
			<li class=" {{ $active == 'fb' ? 'current' : '' }} "><a href="{{ route('/fb/view/{id}', ['id' => $id]) }}"><i c#fb class="fa fa-facebook"></i> Facebook</a></li>
			<li class=" {{ $active == 'tw' ? 'current' : '' }} "><a href="{{ route('/tw/view/{id}', ['id' => $id]) }}"><i c#tw class="fa fa-twitter"></i> Twitter</a></li>
			<li class=" {{ $active == 'in' ? 'current' : '' }} "><a href="{{ route('/in/view/{id}', ['id' => $id]) }}"><i class="fa fa-instagram"></i> Instagram</a></li>
		</ul>
	</div>
</div>
<div class="col-md-9">
	<div class="row">
		<div class="col-md-12">
			<div class="content-box-large">
				<h3>Поиск: {{ $name }}</h3>
			</div>
		</div>
	</div>
	<div class="row grid-wrapper">
		@php
			$i = 0;
		@endphp
		@forelse ($data as $item)
			<div class="col-md-4">
				<div class="content-box-header panel-heading">
					<div class="panel-title ">
						@if ($item->type == 'vk')
							<i c#vk class="fa fa-vk"></i>
						@else
							<i c#tw class="fa fa-twitter"></i>
						@endif
						 <a target="_blank" href="{{ $item->link }}">{{ $item->name }}</a>
					</div>
				</div>
				<div class="content-box-large box-with-header" h=90px>
					<div d:ib w:100p>
						<div d:ib fl ea-j='w=23%'>
							<a target="_blank" href="{{ $item->link }}">
								<img src="{{ $item->photo }}" alt="" >
							</a>
						</div>
						<p m:l fl truncate m:0 p:l ea-j='w=72%'>
							{!! isset($item->city) && $item->city != '' ? $item->city . '<br>' : '' !!}
							{!! isset($item->sex) && $item->sex != '' ? $item->sex . '<br>' : '' !!}
							{!! isset($item->bdate) && $item->bdate != '' ? $item->bdate . '<br>' : '' !!}
						</p>
					</div>
				</div>
			</div>
			@php
				$i++;
			@endphp
			@if ($i % 3 == 0)
				<div class="clearfix"></div>
			@endif
		@empty
			@if ($active == 'fb' || $active == 'in')
				<div class="content-box-large">
					Данное API пока не доступно
				</div>
			@else
				<div class="content-box-large">
					Нет записей
				</div>
			@endif
		@endforelse
	</div>
</div>
{{-- <script>
	window.onload = ()=>{
		var grid = document.querySelector('.grid-wrapper');
		var iso = new Isotope( grid, {
			itemSelector: '.grid-item',
			masonry: {
			}
		});
	}
</script> --}}
@endsection
