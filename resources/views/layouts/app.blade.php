@php
	use App\User;
@endphp
<!DOCTYPE html>
<html lang="en" ea>
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<title>{{ config('app.name', 'Laravel') }}</title>
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link href="http://cdn.umbokc.com/ea/src/ea.css?v=1.3"  rel="stylesheet">
	<link href="/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="/assets/css/styles.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<div class="header">
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<div class="logo">
						<h1><a href="/">{{ config('app.name', 'Laravel') }}</a></h1>
					</div>
				</div>
				<div class="col-md-7">
					<div class="navbar navbar-inverse" role="banner">
						<nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
							<ul class="nav navbar-nav">
								<li><a href="{{ route('/privacy') }}" >Конфиденциальность</a></li>
								@guest
									<li><a href="{{ route('login') }}" >Вход</a></li>
									<li><a href="{{ route('register') }}" >Регистрация</a></li>
								@endguest
								@auth
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ User::curr()->full_name }} <b class="caret"></b></a>
										<ul class="dropdown-menu animated fadeInUp">
											<li><a href="{{ route('/profile') }}" >Личный кабинет</a></li>
											@if (User::isAdmin())
												<li><a href="{{ route('/users') }}" >Пользователи</a></li>
											@endif
											<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
											<li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выйти</a></li>
										</ul>
									</li>
								@endauth
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="page-content container">
		<div class="row">
			@yield('content')
		</div>
	</div>

	<footer>
		<div class="container">
			<div class="copy text-center">
				<p>&copy; {{ config('app.name', 'Laravel') }}. Все права защищены.</p>
			</div>
		</div>
	</footer>

	<script src="http://cdn.umbokc.com/ea/src/ea.js?v=2"></script>
	<script src="https://code.jquery.com/jquery.js"></script>
	<script src="/assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="/assets/js/custom.js"></script>

</body>
</html>
