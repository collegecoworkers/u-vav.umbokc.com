<?php

namespace App;

class RequestIs extends MyModel{

	const VK_API_TOKEN = '76cf47733147ada5b482d38f137bd211dcc4052872449d329397debbff1b7a6c6e28a26260a2513d181ad';

	const TW_OAUTH_ACCESS_TOKEN = '850995553714536448-6fxATEKshz5maVvJIsbbMEvCb9uR9dA';
	const TW_OAUTH_ACCESS_TOKEN_SECRET = 'PEOJqj63ZDk6wlF8z47FRoh2DM9HdSYddKhe6Ne0lDiAO';
	const TW_CONSUMER_KEY = 'BJLEVwSvE7W7BJUNtYo0zQIdt';
	const TW_CONSUMER_SECRET = 'Qsqw8Y8uivg8ywuOvmqzGdh25lyqITh9e8GiW25Fw3Wk51GvGk';

	function search() {
		$this->data_vk = $this->searchVK();
		$this->data_tw = $this->searchTW();
		$this->data_fb = ' ';
		$this->data_in = ' ';
		$this->data_gp = ' ';
	}

	static function oauth() {
		$permissons = [
			'offline'
		];

		$request_params = [
			'client_id' => '5493110',
			'redirect_uri' => 'https://oauth.vk.com/blank.html',
			'response_type' => 'token',
			'display' => 'page',
			'scope' => implode(',',$permissons) 
		];

		$url = 'https://oauth.vk.com/authorize?' . http_build_query($request_params);

		dbg($url);
	}

	function searchVK() {
		// пол
		$sex = [
			'1' => 'женский',
			'2' => 'мужской',
			'0' => 'пол не указан',
		];

		$fields = [
			'photo',
			'sex',
			'city',
			'bdate',
		];

		// параметры для запроса
		$request_params = [
			'v' => '5.74',
			'q' => $this->name,
			'fields' => implode(',', $fields),
			'access_token' => self::VK_API_TOKEN,
		];

		$url = 'https://api.vk.com/method/users.search?' . http_build_query($request_params);

		// оптравка запроса
		$results = file_get_contents($url);
		// декодирование ответа
		$results = json_decode($results);

		// если есть ошибки
		if(isset($results->error)){
			return '';
		}

		$data = $results->response->items;

		array_map(function($item) use ($sex) {
			$item->name = $item->first_name . ' ' . $item->last_name;
			unset($item->first_name);
			unset($item->last_name);
			if(isset($item->sex)) $item->sex = $sex[$item->sex];
			if(isset($item->city)) $item->city = $item->city->title;
		}, $data);

		return json_encode($data);
	}

	function searchTW() {

		$settings = array(
			'oauth_access_token' => self::TW_OAUTH_ACCESS_TOKEN,
			'oauth_access_token_secret' => self::TW_OAUTH_ACCESS_TOKEN_SECRET,
			'consumer_key' => self::TW_CONSUMER_KEY,
			'consumer_secret' => self::TW_CONSUMER_SECRET,
		);

		$url = 'https://api.twitter.com/1.1/users/search.json';
		$getfield = '?q=' . $this->name . '&count=100';
		$requestMethod = 'GET';

		$twitter = new \TwitterAPIExchange($settings);
		$response = $twitter->setGetfield($getfield)->buildOauth($url, $requestMethod)->performRequest();
		$response = json_decode($response);

		// dbg($response, 1);

		if (isset($response->errors)) {
			// dbg("error");
			return '';
		}

		$data = [];
		array_map(function($item) use (&$data) {
			$data[] = [
				'id' => $item->id,
				'id_str' => $item->id_str,
				'name' => $item->name,
				'screen_name' => $item->screen_name,
				'location' => $item->location,
				'image' => $item->profile_image_url,
			];
		}, $response);

		return json_encode($data);
	}

	function getVK(){
		$data = json_decode($this->data_vk);
		if ($data == '') return [];
		array_map(function($item){
			$item->type = 'vk';
			$item->link = 'https://vk.com/id'.$item->id;
		}, $data);

		return $data;
	}

	function getTW(){
		$data = json_decode($this->data_tw);
		if ($data == '') return [];
		array_map(function($item){
			$item->city = $item->location; unset($item->location);
			$item->photo = $item->image; unset($item->image);
			$item->type = 'tw';
			$item->link = 'https://twitter.com/intent/user?user_id='.$item->id;
		}, $data);
		return $data;
	}

	function countAllFs() {
		$d = count($this->getVK());
		$d += count($this->getTW());
		return $d;
	}

}
