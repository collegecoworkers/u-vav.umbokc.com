<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	User,
	Task,
	RequestIs
};

class SiteController extends Controller
{

	function __construct(){
		// $this->middleware('auth');
	}

	function Index() {
		return view('index')->with([]);
	}

	function View($id) {
		$model = RequestIs::getBy('id', $id);
		$data = $model->getVK();
		$data2 = $model->getTW();

		return view('view')->with([
			'id' => $id,
			'name' => $model->name,
			'data' => array_merge($data, $data2),
			'active' => '*',
		]);
	}
	function ViewVK($id) {
		$model = RequestIs::getBy('id', $id);
		$data = $model->getVK();

		return view('view')->with([
			'id' => $id,
			'name' => $model->name,
			'data' => $data,
			'active' => 'vk',
		]);
	}
	function ViewTW($id) {
		$model = RequestIs::getBy('id', $id);
		$data = $model->getTW();

		return view('view')->with([
			'id' => $id,
			'name' => $model->name,
			'data' => $data,
			'active' => 'tw',
		]);
	}
	function ViewFB($id) {
		$model = RequestIs::getBy('id', $id);

		return view('view')->with([
			'id' => $id,
			'name' => $model->name,
			'data' => [],
			'active' => 'fb',
		]);
	}
	function ViewIN($id) {
		$model = RequestIs::getBy('id', $id);

		return view('view')->with([
			'id' => $id,
			'name' => $model->name,
			'data' => [],
			'active' => 'in',
		]);
	}

	function Search(Request $request) {
		$model = new RequestIs();
		$model->name = $request->input('name');
		$model->user_id = is_null(auth()->user()) ? 0 : auth()->user()->id;
		$model->search();
		$model->save();
		return redirect()->to('/view/' . $model->id);
	}

	function Privacy() {
		return view('privacy');
	}

	function Confirm() {
		dbg("this");
		// return view('privacy');
	}

	function Profile() {
		$data = RequestIs::getsBy('user_id', User::curr()->id);
		return view('profile')->with([
			'data' => $data,
		]);
	}
}
